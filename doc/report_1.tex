
%-------------------------------------------------------------------------------
% Document configuration, preparation
%-------------------------------------------------------------------------------

\documentclass[12pt,a4paper]{article}

\usepackage{algpseudocode}
\usepackage{hyperref}

\title{IR Assignment 1 - Report}
\author{Andree Surya (S3390524)\\ Andreas Suekto (S3402775)}
\date{27 August 2013}

\renewcommand*{\familydefault}{\rmdefault}

%-------------------------------------------------------------------------------
% Document's content
%-------------------------------------------------------------------------------

\begin{document}

  \maketitle

  \section{Index Construction}
  \label{sec:index-construction}
    
    The index construction process begin by preparing an in-memory hash map as a 
    mapping table between the lexicon (i.e. term strings) and posting lists. The
    subsequent steps will be to parse the document collection file (i.e. \texttt{latimes})
    given to the program, fetch out and normalize terms from the document, and
    filling in the index.
  
    The given document collection file is represented as SGML, therefore an adjustment has
    to be made to make it XML-compliant and parseable by Java's \emph{Streaming API for XML} 
    (StAX) \cite{harold2003}. The adjustment is done without modifying the original collection 
    file nor creating any temporary file, thus no disk space overhead is introduced. 
    As soon as a new document is encountered, the index construction process will proceed, 
    avoiding the need to load the entire collection into memory, thus minimizing memory footprint.
    
    The next step of the process is the generation of terms from the document. 
    Headline and text content are tokenized with whitespaces as delimiter. Each 
    of these terms will then undergo a term normalization. All numbers and non-letter
    characters within each term will simply be removed from the string without 
    replacement, and then the remaining characters will be case-folded into all 
    lower-case. By adapting this approach, any hypenathed words will simply be
    joined together and any acronyms will be treated as regular words. 
    
    If a stop-words list (see section \ref{sec:stoplist}) is provided to the program, 
    any terms which are listed as a stop-word will not be further processed. All of 
    the remaining terms will then undergo a word stemming process utilizing an implementation
    of Porter stemming algorithm \cite{porter2006}.
    
    After normalized and stemmed terms are obtained, the next step will be to
    calculate terms distribution statistic and update the index. For each term
    encountered, the program will look-up for a corresponding entry in the lexicon
    hash table, and create one if required. A linked list of postings will be updated
    as necessary in the process.
    
    After all of the documents and the corresponding terms are processed, the in-memory
    representation of the document index will be completed. The entire process can
    then be finalized by recording the index into three separate files: \texttt{lexicon},
    \texttt{invlist}, and \texttt{map}. These files will respectively record the 
    lexicon, posting lists, and document number map, which together represents
    the complete document index.
    
  \section{Stoplist}
  \label{sec:stoplist}
  
    Within the program, the stoplist will be stored in a custom implementation of 
    hash set. The initial capacity of the hash buckets are adjusted to the number 
    of stop-words in the provided sample stoplist file, but able to be expanded as
    soon as the number of elements exceeded a certain threshold. The load factor is set 
    to be 0.75, as it offers a good trade-off between time and space costs 
    \cite{javadoc_hashtable2013}. The expansion process will double the number of 
    buckets and simply reposition all elements by recalculating the hash code of each.
    
    The hash function used to calculate string's hash code is a \emph{Cyclic shift hash
    function} \cite{goodrich2008}. This function utilizes a series of circular bit 
    shift operation to permutate bit sequences. The operations are performed 
    to all characters within the string, with the position of characters taken into 
    consideration. This function is adopted for the reason of efficiency, compared to the 
    default hash code calculation of string in Java, which involves a series of 
    multiplication and exponentiation operation \cite{javadoc_string2013}. As
    according to empirical observation in \cite{goodrich2008}, a Cyclic shift hash 
    function with 5 shifts per characters, similar to the one implemented in this 
    program, can minimize the number of collisions for the tested 25,000 English words to 4.
    
    After the hash code of the string is obtained, it will be passed to a 
    \emph{Multiply-Add-Divide} (MAD) function \cite{goodrich2008} to calculate 
    bucket position. MAD is utilized due to its efficiency, pseudorandom property, 
    and ability to produce uniform distribution amoung buckets. To handle the case 
    of bucket entries collision, a separate chaining scheme with simple linked list 
    is adopted.
    
  \section{Index Search}
  \label{sec:index-search}
  
    During the initiation of the search program, the whole lexicon file will be
    loaded from disk and stored in memory as a hash mapping between term string and pointer 
    to a position in the inverted list file. The same goes for the document map file,
    which is a mapping between document ID and document number string. The implementation of 
    hash table and hashing scheme is similar to the one adopted for storing stoplist. 
    Please refer to section \ref{sec:stoplist} for further detail.
    
    Each of the query terms given to the program will undergo the same normalization 
    and stemming process as adopted by the index construction program 
    (see section \ref{sec:index-construction}). After normalization, each term 
    will be looked-up against the lexicon hash table for a corresponding entry, 
    that is, pointer to the inverted list file. If exists, the pointer will then be 
    used to fetch out a corresponding posting list from this file. The posting list 
    can then be presented to the user as a series of document numbers and within-document 
    frequencies. This document number can be easily obtained from document ID by referring 
    to the document number hash map discussed previously.
  
  \section{Index Size}
  \label{sec:index-size}
  
    Without compression, the size of the inverted list file is 154 MB. Added with
    5 MB for the size of the lexicon file, it is still about one-third compared to 
    the original document collection file with size 475 MB. It is smaller than the
    original collection file because of their compact representation. Occurence of
    terms within documents are simply represented as a set of integers, one for
    each unique term within the document.
  
  \section{Compression}
  \label{sec:compression}
  
    A \emph{Variable Byte} (VB) encoding scheme \cite{manning2008} is utilized to
    compress the inverted list file. The encoding scheme is adopted because of its 
    efficiency and good average compression ratio.
    
    The natural ordering of document ID within a posting list is exploited by the
    VB compression scheme. Only the difference between one ID to the next is stored
    within the inverted list file, minimizing the amount of bytes required to
    represent each ID. 
    
    As for within-document frequency, the same cascading approach cannot be adopted 
    due to its unordered nature within a posting list. However, due to the relatively 
    small number for within-document frequency from the given document collection file, 
    most can be represented with a single byte.
    
    With compression applied, the size of the inverted list file is 44 MB, about 
    one-third ratio compared to its plain counterpart.
  
  \section{Contribution}
  \label{sec:contribution}
  
    Andreas is responsible for the design and implementation of hash map, hash set, 
    index generation module, VB compression and decompression tool.
    
    Andree is responsible for the establishment of program structure, overall 
    refactoring of code, document collection parsing, implementation of Porter 
    stemming algorithm, and the search module.

  \begin{thebibliography}{9}

    \bibitem{harold2003}
      Harold, E.R 2003, \emph{An introduction to StAX}, accessed 27 August 2013,
      \url{http://www.xml.com/lpt/a/1287}

    \bibitem{porter2006}      
      Porter, M 2006, \emph{The Porter stemming algorithm}, accessed 27 August 2013,
      \url{http://tartarus.org/martin/PorterStemmer}

    \bibitem{manning2008}
      Manning, C.D, Raghavan, P and Schütze, H 2008, \emph{An introduction to information
      retrieval}, ch. 5, Cambridge University Press, England.
      
    \bibitem{goodrich2008}
      Goodrich, M.T, Tamassia, R 2008, \emph{Data structures and algorithms in Java},
      John Wiley and Sons, New Jersey.
      
    \bibitem{javadoc_hashtable2013}
      \emph{Hashtable}, Java SE 7 Documentation, accessed 27 August 2013,
      \url{http://docs.oracle.com/javase/7/docs/api/java/util/Hashtable.html}
      
    \bibitem{javadoc_string2013}
      \emph{String}, Java SE 7 Documentation, accessed 27 August 2013,
      \url{http://docs.oracle.com/javase/7/docs/api/java/lang/String.html}

  \end{thebibliography}

\end{document}

