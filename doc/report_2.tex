
%-------------------------------------------------------------------------------
% Document configuration, preparation
%-------------------------------------------------------------------------------

\documentclass[12pt,a4paper]{article}

\usepackage{hyperref}
\usepackage{mdwlist}

\title{IR Assignment 2 - Report}
\author{Andree Surya (S3390524)\\ Andreas Suekto (S3402775)}
\date{16 October 2013}

\renewcommand*{\familydefault}{\rmdefault}

%-------------------------------------------------------------------------------
% Document's content
%-------------------------------------------------------------------------------

\begin{document}

  \maketitle

  The Information Retrieval program made for assignment 1 has been adapted and
  modified for assignment 2 to support ranked retrieval functionality and 
  automatic query expansion. This report will emphasize on the adjustments that 
  have been made. Algorithms and data structures that have been discussed in 
  previous report \cite{suekto2013} will not be elaborated in detail.

  \section{Ranked Retrieval}
  \label{sec:ranked-retrieval}
  
    The \texttt{Index} program has been modified to record the weights of each 
    indexed documents to a map file. These weights are essentially the length of 
    a document, normalized to the average document length within the collection:
    %
    \begin{equation}
      K_d = k_1 \cdot \left( (1 - b) + \frac{b \cdot L_d}{AL} \right)
      \label{eq:document-weight}
    \end{equation}
    %
    where:
    \begin{itemize*}
      \item \(K_d\) is the document's weight.
      \item \(L_d\) is the document's length in characters.
      \item \(AL\) is the average document length within the collection.
      \item \(k_1\) and \(b\) are parameters set to \(1.2\) and \(0.75\), respectively.
    \end{itemize*}
    
    The \texttt{Search} program will tokenize, normalize, and stem user's query string
    using the same \texttt{TermsGenerator} module utilized by the \texttt{Index} program.
    The resulting set of terms will be queried against the inverted index to search 
    for matching documents. Similarity score between a query string and a document
    is calculated using a simplified Okapi BM25 similarity function 
    \cite{manning2008}\cite{scholer2013b}:
    %
    \begin{equation}
      BM25(Q, D_d) = \sum\limits_{t \in Q} 
          \log \left( \frac{N - f_t + 0.5}{f_t + 0.5} \right) 
          \cdot \frac{(k_1 + 1) \cdot f_{d,t}}{K + f_{d,t}}
    \end{equation}
    %
    where:
    \begin{itemize*}
      \item \(Q\) is a query consisting of terms \(t\).
      \item \(D_d\) is a document in the collection.
      \item \(N\) is the number of documents in the collection.
      \item \(f_{d,t}\) is the number of occurences of \(t\) in \(D_d\).
      \item \(f_t\) is the number of documents containing term \(t\).
      \item \(k_1\) is a parameter set to \(1.2\).
      \item \(K_d\) is the weight of document \(D_d\) calculated at indexing time.
    \end{itemize*}
    
    For the sake of efficiency, query string will be processed one term at a time, 
    using a score \emph{Accumulators} and the query processing procedures described in 
    IR Lecture 3 \cite{scholer2013a}, pp. 61-64. The score accumulator is implemented 
    using a hash table \cite{suekto2013} for quick mapping between document ID and 
    partial similarity score.
    
    After all query terms have been processed and complete similarity scores have
    been obtained, the documents with the highest similarity scores will be collected
    using a \emph{min-heap} data structure and \emph{heapify} procedure as described 
    in IR Lecture 3 \cite{scholer2013a}, pp. 66-71. These top documents will then 
    be sorted in respect to similarity scores and be presented to the user.
    
  \section{Advanced Information Retrieval Feature}
  \label{sec:advanced-ir-feature}
  
    The advanced IR feature chosen to be implemented in this work is the automatic
    query expansion feature, i.e. pseudo-relevance feedback. The feature is activated
    when the \texttt{Search} program is supplied with parameter \(R\), the number of
    top documents whose terms will be considered for query expansion.
    
    The query expansion process is implemented using the procedure described in 
    IR Lecture 5 \cite{scholer2013c}, pp. 30-33:

    \begin{enumerate}
      \item Using the user-supplied query terms, perform initial search to fetch 
            top \(R\) documents. These documents are assumed to be relevant to the 
            search and all terms within will be assigned as candidate terms for 
            query expansion.
            
      \item Tokenize all \(R\) document into terms.
      
      \item Calculate \(r_t\) for each candidate terms. \(r_t\) is the number of 
            documents in which term \(t\) occurs within the top \(R\) relevant documents. 
            These counters are stored in a hash table for quick mapping between term 
            string and counter.
            
      \item Calculate the \emph{term-selection value} (TSV) value for each candidate
            terms using the following formula:
            %
            \begin{equation}
              TSV_t = \left( \frac{f_t}{N} \right)^{r_t} \frac{R!}{r_t!\,(R - r_t)!}
            \end{equation}
            
      \item Select \(E\) terms with minimum TSV value using a \emph{max-heap} data
            structure \cite{scholer2013a}. \(E\) is a user-supplied parameter with
            the default value of 25.
            
      \item Eliminate terms redundancy by removing terms that are already available 
            in the original user-supplied query terms.
    \end{enumerate}
    
    The resulting extra terms are assumed to be representative to the top \(R\)
    documents. These extra terms, together with the original query terms, will
    be provided to the second search run to retrieve the final search results.
    The similarity score of the extra terms are weighted down to a factor of
    \(0.2\) to minimize the risk of \emph{query drift}.
  
  \section{Contribution}
  \label{sec:contribution}

  \begin{thebibliography}{9}

    \bibitem{manning2008}
      Manning, C.D, Raghavan, P and Schütze, H 2008, \emph{An introduction to 
      information retrieval}, ch. 5, Cambridge University Press, England.
      
    \bibitem{scholer2013a}
      Scholer, F 2013, \emph{Information Retrieval Lecture - Retrieval Models I},
      Royal Melbourne Institute of Technology, Melbourne.
      
    \bibitem{scholer2013b}
      Scholer, F 2013, \emph{Information Retrieval Lecture - Retrieval Models II},
      Royal Melbourne Institute of Technology, Melbourne.
      
    \bibitem{scholer2013c}
      Scholer, F 2013, \emph{Information Retrieval Lecture - Query Expansion and
      Relevance Feedback}, Royal Melbourne Institute of Technology, Melbourne.
      
    \bibitem{suekto2013}
      Suekto, A, Surya, A 2013, \emph{IR Assignment 1 - Report}, Royal Melbourne
      Institute of Technology, Melbourne.

  \end{thebibliography}

\end{document}

