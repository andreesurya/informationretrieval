
import static org.junit.Assert.*;

import index.CompressedPostingFile;
import index.Posting;
import index.PostingFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CompressedPostingFileTest {

    private PostingFile postingFile;
    
    @Before
    public void setUp() throws Exception {
        String fileName = File.createTempFile("invlist_", null).getName();
        postingFile = new CompressedPostingFile(fileName);
    }

    @After
    public void tearDown() throws Exception {
        postingFile.close();
    }

    @Test
    public void test() throws IOException {
        List<Posting> output = new ArrayList<Posting>();
        output.add(new Posting(1000, 5));
        output.add(new Posting(1128, 9));
        output.add(new Posting(1255, 30));
        output.add(new Posting(17639, 2));
        output.add(new Posting(100083, 10));
        
        postingFile.writePostings(output, 0);
        
        List<Posting> input = postingFile.readPostings(0);
        assertEquals(output.size(), input.size());
        
        for (int i = 0; i < output.size(); i++) {
            Posting outPost = output.get(i);
            Posting inPost = input.get(i);
            
            assertEquals(outPost.getDocID(), inPost.getDocID());
            assertEquals(outPost.getFrequency(), inPost.getFrequency());
        }
    }

}
