

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;

public class Evaluation {
	final static String DELIM = " ";
	
	public static class QueryResultRecord {
		String docID;
		int rank;
		double score;
		
		public QueryResultRecord (String docID, int rank, double score) {
			this.docID = docID;
			this.rank = rank;
			this.score = score;
		}
	}

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception 
	{
		if (args.length < 2)
		{
			System.out.println("Not enough arguments.");
			return;
		}
		String qrelDocPath = args[0]; // The relevance score from human observation
		String queryDocPath = args[1]; // the queries to test
		
		String normalSearchPath = "normal_";
		String expandSearchPath = "expand_";
		
		HashMap<Integer, HashMap<String, Boolean>> 
				qrel 
				= new HashMap<Integer, HashMap<String, Boolean>>();
		HashMap<Integer, ArrayList<QueryResultRecord>> 
				normalSearch 
				= new HashMap<Integer, ArrayList<QueryResultRecord>>();
		HashMap<Integer, ArrayList<QueryResultRecord>> 
				expandSearch 
				= new HashMap<Integer, ArrayList<QueryResultRecord>>();
		ArrayList<Integer> queryCodes
				= new ArrayList<Integer>();
		
		// Fill in the qrel
		fillRelevanceData(qrel, qrelDocPath);
		fillQueryCodes(queryCodes, queryDocPath);
		
		// Fill in the Search Result data structure
		for (Integer key : queryCodes) {
			String path;
			path = normalSearchPath + key;
			fillSearchResult(normalSearch, path);
			path = expandSearchPath + key;
			fillSearchResult(expandSearch, path);
		}
		
		System.out.println("\n\n=======P@10 Normal Search=======");
		precisionAtTen(qrel, normalSearch);
		System.out.println("\n\n=======P@10 Expand Search=======");
		precisionAtTen(qrel, expandSearch);
		
//		System.out.println("\n\n=======MAP Normal Search=======");
//		meanAveragePrecision(qrel, normalSearch);
//		System.out.println("\n\n=======MAP Expand Search=======");
//		meanAveragePrecision(qrel, expandSearch);
		
		System.out.println("\n\n=======nDCG Normal Search=======");
		nDCG(qrel, normalSearch);
		System.out.println("\n\n=======nDCG Expand Search=======");
		nDCG(qrel, expandSearch);
	}
	
//	public static void main (String[] args)
//	{
//		ArrayList<Integer> gainList = new ArrayList<Integer>();
//		gainList.add(3);
//		gainList.add(2);
//		gainList.add(3);
//		gainList.add(0);
//		gainList.add(0);
//		gainList.add(1);
//		gainList.add(2);
//		gainList.add(3);
//		gainList.add(0);
//		gainList.add(0);
//		
//		System.out.println(getDCG(gainList));
//	}
	
	public static void nDCG (HashMap<Integer, HashMap<String, Boolean>> qrel, 
			   				 HashMap<Integer, ArrayList<QueryResultRecord>> searchResult) throws Exception
	{
		Set<Integer> qLabels = searchResult.keySet();
		ArrayList<Double> nDCGs = new ArrayList<Double>();
		for (Integer qLabel : qLabels)
		{
			// Get the ideal DCG
			HashMap<String, Boolean> relevances = qrel.get(qLabel);
			int totalRelatedDoc = totalRelatedDoc(relevances);
			double idealDCG = getIdealDCG(totalRelatedDoc, searchResult.get(qLabel).size());
			
			// Get Search DCG
			ArrayList<Integer> gainList = generateGainList(qrel, searchResult, qLabel);
			double DCG = getDCG(gainList);
			
			// Calculate normalized DCG
			double nDCG = DCG / idealDCG;
			nDCGs.add(nDCG);
			
			// print-out
			System.out.println("    "+qLabel+" : "+nDCG);
		}
		
		// print out for copy to excel
		for (int i = 0; i < nDCGs.size(); i++)
			System.out.println(nDCGs.get(i));
	}
	
	public static double getIdealDCG (int totalRelatedDocument, int maxSize)
	{
		ArrayList<Integer> gainList = new ArrayList<Integer>();
		
		for (int i = 0; i < totalRelatedDocument; i++)
			gainList.add(1);
		for (int i=totalRelatedDocument; i < maxSize; i++)
			gainList.add(0);
		
		return getDCG(gainList);
	}
	
	public static ArrayList<Integer> generateGainList (HashMap<Integer, HashMap<String, Boolean>> qrel, 
													   HashMap<Integer, ArrayList<QueryResultRecord>> searchResult,
													   Integer qLabel) throws Exception
	{
		ArrayList<Integer> gainList = new ArrayList<Integer>();
		HashMap<String, Boolean> relevances = qrel.get(qLabel);
		ArrayList<QueryResultRecord> results = searchResult.get(qLabel);
		
		int rankChecker = -1; // while this assume the searchResult is already ordered.
		QueryResultRecord record;
		for (int i = 0; i < results.size(); i++)
		{
			record = results.get(i);
			Boolean isRelevance = relevances.get(record.docID); 
			if (isRelevance != null && isRelevance)
				gainList.add(1);
			else
				gainList.add(0);
			
			if (record.rank < rankChecker)
				throw new Exception ("Search Result not in order");
			
			rankChecker = record.rank;
		}
		
		return gainList;
	}
	
	public static double getDCG (ArrayList<Integer> gainList)
	{
		double DCG = 0.0;
		double numerator;
		double denominator;
		int b = 2;
		
		for (int i = 0; i < gainList.size(); i++)
		{
			numerator = gainList.get(i);
			denominator = 1 + (Math.log(i+1)/Math.log(b));
			DCG += (numerator / denominator);
		}
		
		return DCG;
	}
	
	public static double meanAveragePrecision(HashMap<Integer, HashMap<String, Boolean>> qrel,
												HashMap<Integer, ArrayList<QueryResultRecord>> search)
	{
		double map = 0.0;
		int numOfQuery = 0;
		double ap = 0.0;
		Set<Integer> keys = search.keySet();
		for (Integer key : keys)
		{
			numOfQuery++;
			ap = averagePrecision(qrel.get(key), search.get(key));
			map += ap;
			System.out.println("    "+key + " : " + ap);
		}
		map = map / numOfQuery;
		System.out.println("MAP : "+ map);
		return map;
	}
	
	public static double averagePrecision(HashMap<String, Boolean> qrel,
											ArrayList<QueryResultRecord> search)
	{
		int totalRelatedDocument = totalRelatedDoc(qrel);
		
		int correctAnswer = 0;
		double sumOfPrecision = 0.0;
		sortBasedOnRank(search);
		for (int i = 0; i < search.size(); i++)
		{
			Boolean isRelevance = qrel.get(search.get(i).docID);
			if (isRelevance != null && isRelevance == true)
			{
				correctAnswer++;
				sumOfPrecision += (((double)correctAnswer)/(i+1));
			}
		}
		return (sumOfPrecision / totalRelatedDocument);
	}
	
	public static void sortBasedOnRank (ArrayList<QueryResultRecord> search)
	{
		ArrayList<QueryResultRecord> sorted = new ArrayList<Evaluation.QueryResultRecord>();
		for (int i = 0; i < search.size(); i++)
		{
			int index = -1;
			for (int j = 0; j < search.size(); j++)
			{
				if (search.get(j).rank == (i+1))
				{
					index = j;
					break;
				}
			}
			sorted.add(search.get(index));
		}
		
		search.removeAll(sorted);
		if (search.isEmpty())
		{
			for (int i = 0; i < sorted.size(); i++)
				search.add(sorted.get(i));
		}
		else
			throw new RuntimeException("Search is not empty!!!");
	}
	
	public static void precisionAtTen (HashMap<Integer, HashMap<String, Boolean>> qrel,
										HashMap<Integer, ArrayList<QueryResultRecord>> search)
	{
		Set<Integer> keys = search.keySet();
		ArrayList<Double> prec10vals = new ArrayList<Double>();
		
		for (Integer key : keys)
		{
			ArrayList<QueryResultRecord> listOfAnswer = search.get(key);
			HashMap<String, Boolean> relevanceMap = qrel.get(key);
			System.out.println("-----Precision @ 10 : "+ key +"-----");
			ArrayList<QueryResultRecord> topten = new ArrayList<Evaluation.QueryResultRecord>();
			
			// get the top ten
			int i = 0;
			while (i < 10 && i < listOfAnswer.size())
			{
				for (QueryResultRecord r : listOfAnswer)
				{
					if (r.rank == (i+1))
					{
						topten.add(r);
						break;
					}
				}
				i++;
			}
			
			int correctAnswerReturned = 0;
			for (i = 0; i < topten.size(); i++)
			{
				Boolean isRelevance = relevanceMap.get(topten.get(i).docID);
				if (isRelevance != null && isRelevance == true)
				{
					correctAnswerReturned++;
				}
			}
			
			int totalRelevanceDoc = totalRelatedDoc(relevanceMap);
			
			System.out.println("     Correct answer : "+correctAnswerReturned);
			System.out.println("     Top Ten Size   : "+topten.size());
			if (topten.size() != 10)
			{
				//throw new RuntimeException("ANSWER LESS THAN 10, CAN'T DO P@10");
			}
			System.out.println("     P@10 : " + ((double)correctAnswerReturned)/10);
			prec10vals.add(((double)correctAnswerReturned)/10);
			System.out.println("     Total Relevance Document   : " + totalRelevanceDoc);
		}
		
		for (int i = 0; i < prec10vals.size(); i++)
		{
			System.out.println(prec10vals.get(i));
		}
	}
	
	public static int totalRelatedDoc (HashMap<String, Boolean> relevanceMap)
	{
		int totalRelevanceDoc = 0;
		Set<String> docs = relevanceMap.keySet();
		for (String doc : docs)
		{
			if (relevanceMap.get(doc) == true)
				totalRelevanceDoc++;
		}
		return totalRelevanceDoc;
	}
	
	public static void printAllStructure (HashMap<Integer, HashMap<String, Boolean>> qrel,
											HashMap<Integer, ArrayList<QueryResultRecord>> normalSearch,
											HashMap<Integer, ArrayList<QueryResultRecord>> expandSearch)
	{
		System.out.println("===============RELEVANCE=================");
		printRelStructure(qrel);
		System.out.println("===============NORMAL=================");
		printSearchStructure(normalSearch);
		System.out.println("===============EXPAND=================");
		printSearchStructure(expandSearch);
	}
	
	public static void printRelStructure (HashMap<Integer, HashMap<String, Boolean>> qrel)
	{
		Set<Integer> keys = qrel.keySet();
		HashMap<String, Boolean> infos;
		for (Integer key : keys) {
			infos = qrel.get(key);
			Set<String> docs = infos.keySet();
			for (String doc : docs) {

				System.out.println(key + " " + doc + " " + infos.get(doc));
			}
		}
	}
	
	public static void printSearchStructure 
						(HashMap<Integer, ArrayList<QueryResultRecord>> search)
	{
		Set<Integer> keys = search.keySet();
		ArrayList<QueryResultRecord> recordInfos;
		for (Integer key : keys) {
			recordInfos = search.get(key);
			for (int i = 0; i < recordInfos.size(); i++) {

				System.out.println(key + " " + recordInfos.get(i).docID 
									   + " " + recordInfos.get(i).rank
									   + " " + recordInfos.get(i).score);
			}
		}
	}
	public static void fillQueryCodes (ArrayList<Integer> queryCodes, String queryPath) throws FileNotFoundException
	{
		File qrelFile = new File(queryPath);
		Scanner scanner = new Scanner(qrelFile);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			StringTokenizer tokenizer = new StringTokenizer(line, DELIM);
			int queryCode = Integer.parseInt(tokenizer.nextToken());
			queryCodes.add(queryCode);
		}
		scanner.close();
	}

	public static void fillSearchResult(HashMap<Integer, ArrayList<QueryResultRecord>> search
								, String searchDocPath) throws FileNotFoundException 
	{
		File qrelFile = new File(searchDocPath);
		Scanner scanner = new Scanner(qrelFile);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			StringTokenizer tokenizer = new StringTokenizer(line, DELIM);
			int queryCode = Integer.parseInt(tokenizer.nextToken());
			QueryResultRecord pairInfo = new QueryResultRecord(tokenizer.nextToken(), 
														Integer.parseInt(tokenizer.nextToken()),
														Double.parseDouble(tokenizer.nextToken()));
			if (search.get(queryCode) == null) {
				search.put(queryCode, new ArrayList<QueryResultRecord>());
			}
			search.get(queryCode).add(pairInfo);
		}
		scanner.close();
	}
	
	
	public static void fillRelevanceData(HashMap<Integer, HashMap<String, Boolean>> qrel
			, String qrelDocPath) throws FileNotFoundException 
	{
		File qrelFile = new File(qrelDocPath);
		Scanner scanner = new Scanner(qrelFile);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			StringTokenizer tokenizer = new StringTokenizer(line, DELIM);
			int queryCode = Integer.parseInt(tokenizer.nextToken());
			tokenizer.nextToken();
			if (qrel.get(queryCode) == null) {
				qrel.put(queryCode, new HashMap<String, Boolean>());
			}
			String docID = tokenizer.nextToken();
			Boolean isRelevance = false;
			if ((Integer.parseInt(tokenizer.nextToken())) == 1) {
				isRelevance = true;
			}
			qrel.get(queryCode).put(docID, isRelevance);
		}
		scanner.close();
	}
}
