

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import data_structure.*;

/**
 * UnitTest Workbench for MinHeap
 * 
 * Test Whether the MinHeap could get the Top Set of data.
 * Answer sheet created by Sorting the Collection using
 * java's Collections.sort()
 */
public class MinHeapTest {

	public final static int SIZE = 100;
	public final static int TOP = 10;
	
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) 
	{
		System.out.println("============================== TEST MIN HEAP ==============================");
		System.out.println("DATA SIZE : " + SIZE);
		System.out.println("TOP  SIZE : " + SIZE +"\n");
		ArrayList<DocumentScore> docList = generateList(SIZE);
		MinHeap minHeap = new MinHeap(TOP);
		
		// Heap Top extraction
		for (int i = 0; i < docList.size(); i++)
			minHeap.insert(docList.get(i));
		Comparable[] heapResult = minHeap.get();
		
		// Get Answer Sheet
		Collections.sort(docList);
		ArrayList<DocumentScore> topList = new ArrayList<DocumentScore>();
		for (int i = 0; i < TOP; i++)
		{
			topList.add(docList.get(docList.size()-1-i));
		}
		
		// Check
		boolean isSuccess = true;
		for (int i = 0; i < TOP; i++)
		{
			if (!topList.contains(heapResult[i]))
			{
				isSuccess = false;
				break;
			}
			
			System.out.printf("%2d. %-35s = %-35s\n", i
												,topList.get(topList.indexOf(heapResult[i]))
												, heapResult[i]);
		}
		
		System.out.println("==========================================================================");
		// print result
		if (isSuccess)
			System.out.println("SUCCESFULL MIN HEAP TOP " + TOP);
		else
			System.out.println("ERROR MIN HEAP TOP " + TOP);
	}

	public static ArrayList<DocumentScore> generateList (int size)
	{
		ArrayList<DocumentScore> docList = new ArrayList<DocumentScore>(size);
		Random rand = new Random(System.currentTimeMillis());
		
		for (int i = 0; i < size; i++)
			docList.add(new DocumentScore(rand.nextInt(), rand.nextDouble()));
		
		return docList;
	}
}
