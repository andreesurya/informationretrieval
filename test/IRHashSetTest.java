
import static org.junit.Assert.*;

import hash.IRHashSet;
import hash.StringHashCoder;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;



public class IRHashSetTest {
    private static final int CAPACITY = 100;
    private Set<String> hashSet;
    
    @Before
    public void setUp() throws Exception {
        hashSet = new IRHashSet<String>(CAPACITY, new StringHashCoder());
    }

    @Test // Test set's behavior under overloaded capacity.
    public void testOverCapacity() {
        final int OVERFLOW_FACTOR = 100;
        
        for (int i = 0; i < CAPACITY * OVERFLOW_FACTOR; i += 2) {
            String element = "element" + i;
            
            assertFalse(hashSet.contains(element));
            hashSet.add("element" + i);
            assertTrue(hashSet.contains("element" + i));
        }
    }

}
