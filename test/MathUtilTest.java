
import static org.junit.Assert.*;

import org.junit.Test;

import util.MathUtil;


public class MathUtilTest {
    @Test
    public void testCombination() {
        assertEquals(2, MathUtil.combination(2, 1));
        assertEquals(3, MathUtil.combination(3, 2));
        assertEquals(252, MathUtil.combination(10, 5));
        assertEquals(1, MathUtil.combination(10, 10));
        assertEquals(184756, MathUtil.combination(20, 10));
    }
}
