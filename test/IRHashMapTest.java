
import static org.junit.Assert.*;

import hash.IRHashMap;
import hash.StringHashCoder;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class IRHashMapTest {
    private static final int CAPACITY = 100;
    private Map<String, String> hashMap;
    
    @Before
    public void setUp() throws Exception {
        hashMap = new IRHashMap<String, String>(CAPACITY, new StringHashCoder());
    }

    @Test // Test map's behavior under overloaded capacity
    public void testOverCapacity() {
        final int OVERFLOW_FACTOR = 100;
        
        // Test adding items.
        for (int i = 0; i < CAPACITY * OVERFLOW_FACTOR; i += 2) {
            String key = "key" + i;
            String value = "value" + i;
            
            hashMap.put(key, value);
            assertTrue(hashMap.containsKey(key));
            assertEquals(value, hashMap.get(key));
        }

        // Test to get non-existent items.
        for (int i = 1; i < CAPACITY * OVERFLOW_FACTOR; i += 2) {
            String key = "key" + i;
            
            assertFalse(hashMap.containsKey(key));
            assertNull(hashMap.get(key));
        }
    }
    
    @Test // Test map's values() function
    public void testValues() {
        final int SIZE = 1000;
        for (int i = 0; i < SIZE; i++)
            hashMap.put("key" + i, "value" + i);
        
        assertEquals(SIZE, hashMap.values().size());
    }
}
