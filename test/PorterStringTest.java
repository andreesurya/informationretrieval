
import static org.junit.Assert.*;

import org.junit.Test;

import stem.PorterString;


public class PorterStringTest {
    @Test
    public void testHasSuffix() {
        assertTrue(new PorterString("words").hasSuffix("s"));
        assertTrue(new PorterString("blesses").hasSuffix("sses"));
        assertTrue(new PorterString("infliction").hasSuffix("ion"));
        
        assertFalse(new PorterString("elaboration").hasSuffix("nation"));
        assertFalse(new PorterString("jewelry").hasSuffix("ly"));
        assertFalse(new PorterString("kisses").hasSuffix("ed"));
    }
    
    @Test
    public void testHasDoubleConsonantEnd() {
        assertTrue(new PorterString("butt").hasDoubleConsonantEnd());
        assertTrue(new PorterString("fall").hasDoubleConsonantEnd());
        assertTrue(new PorterString("success").hasDoubleConsonantEnd());
        assertTrue(new PorterString("decisiveness").hasDoubleConsonantEnd());
        
        assertFalse(new PorterString("stemmer").hasDoubleConsonantEnd());
        assertFalse(new PorterString("programmer").hasDoubleConsonantEnd());
        assertFalse(new PorterString("failure").hasDoubleConsonantEnd());
        assertFalse(new PorterString("retrieval").hasDoubleConsonantEnd());
    }
    
    @Test
    public void testHasCvcEnd() {
        assertTrue(new PorterString("illustrated").hasCvcEnd());
        assertTrue(new PorterString("milestones").hasCvcEnd());
        assertTrue(new PorterString("mistaken").hasCvcEnd());
        assertTrue(new PorterString("debates").hasCvcEnd());
        assertTrue(new PorterString("minced").hasCvcEnd());
        
        assertFalse(new PorterString("trow").hasCvcEnd());
        assertFalse(new PorterString("tax").hasCvcEnd());
        assertFalse(new PorterString("tray").hasCvcEnd());
        assertFalse(new PorterString("illustrate").hasCvcEnd());
        assertFalse(new PorterString("mistake").hasCvcEnd());
    }

    @Test
    public void testHasVowel() {
        assertTrue(new PorterString("one").hasVowel());
        assertTrue(new PorterString("two").hasVowel());
        assertTrue(new PorterString("cry").hasVowel());
        
        assertFalse(new PorterString("blrgh").hasVowel());
        assertFalse(new PorterString("yvnn").hasVowel());
        assertFalse(new PorterString("mlbrn").hasVowel());
    }
    
    @Test
    public void testIsConsonant() {
        PorterString word = new PorterString("yvonne");
        assertTrue(word.isConsonant(0));
        assertTrue(word.isConsonant(1));
        assertFalse(word.isConsonant(2));
        assertTrue(word.isConsonant(3));
        assertTrue(word.isConsonant(4));
        assertFalse(word.isConsonant(5));
        
        word = new PorterString("freely");
        assertTrue(word.isConsonant(0));
        assertTrue(word.isConsonant(1));
        assertFalse(word.isConsonant(2));
        assertFalse(word.isConsonant(3));
        assertTrue(word.isConsonant(4));
        assertFalse(word.isConsonant(5));
        
        word = new PorterString("torture");
        assertTrue(word.isConsonant(0));
        assertFalse(word.isConsonant(1));
        assertTrue(word.isConsonant(2));
        assertTrue(word.isConsonant(3));
        assertFalse(word.isConsonant(4));
        assertTrue(word.isConsonant(5));
        assertFalse(word.isConsonant(6));
    }
    
    @Test
    public void testCalculateM() {
        assertEquals(0, new PorterString("tr").countM());
        assertEquals(0, new PorterString("ee").countM());
        assertEquals(0, new PorterString("tree").countM());
        assertEquals(0, new PorterString("by").countM());
        
        assertEquals(1, new PorterString("trouble").countM());
        assertEquals(1, new PorterString("oats").countM());
        assertEquals(1, new PorterString("trees").countM());
        assertEquals(1, new PorterString("ivy").countM());
        assertEquals(1, new PorterString("feed").countM());
        
        assertEquals(2, new PorterString("troubles").countM());
        assertEquals(2, new PorterString("private").countM());
        assertEquals(2, new PorterString("oaten").countM());
        assertEquals(2, new PorterString("orrery").countM());
        
        assertEquals(3, new PorterString("insatiate").countM());
        assertEquals(3, new PorterString("reformed").countM());
        assertEquals(3, new PorterString("remedies").countM());
        assertEquals(3, new PorterString("whosoever").countM());
    }
}
