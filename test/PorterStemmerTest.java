
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


import org.junit.Before;
import org.junit.Test;

import stem.PorterStemmer;


public class PorterStemmerTest {
    private PorterStemmer stemmer;
    
    @Before
    public void setUp() throws Exception {
        stemmer = new PorterStemmer();
    }
    
    @Test
    public void testStemWordFile() throws Exception {
        InputStream in = new FileInputStream("data/stem_fixture.csv");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        
        String line = reader.readLine();
        int sampleCount = 0;
        int incorrectCount = 0;
        
        while (line != null) {
            StringTokenizer tokenizer = new StringTokenizer(line, ",");
            String original = tokenizer.nextToken();
            String expected = tokenizer.nextToken();
            
            CharSequence stemmed = stemmer.stemWord(original);
            if (! expected.equals(stemmed)) incorrectCount++;
            
            sampleCount++;
            line = reader.readLine();
        }
        
        double incorrectRate = incorrectCount / (double) sampleCount;
        assertTrue("Less than 1% incorrect rate", incorrectRate < 0.01);
        reader.close();
    }
    
    @Test
    public void testStemWord() {
        assertEquals("caress", stemmer.stemWord("caresses"));
        assertEquals("poni", stemmer.stemWord("ponies"));
        assertEquals("ti", stemmer.stemWord("ties"));
        assertEquals("caress", stemmer.stemWord("caress"));
        assertEquals("cat", stemmer.stemWord("cats"));
        assertEquals("feed", stemmer.stemWord("feed"));
        assertEquals("agre", stemmer.stemWord("agreed"));
        assertEquals("plaster", stemmer.stemWord("plastered"));
        assertEquals("bled", stemmer.stemWord("bled"));
        assertEquals("monitor", stemmer.stemWord("monitoring"));
        assertEquals("sing", stemmer.stemWord("sing"));
        assertEquals("troubl", stemmer.stemWord("troubled"));
        assertEquals("size", stemmer.stemWord("sized"));
        assertEquals("hop", stemmer.stemWord("hopping"));
        assertEquals("tan", stemmer.stemWord("tanned"));
        assertEquals("fall", stemmer.stemWord("falling"));
        assertEquals("hiss", stemmer.stemWord("hissing"));
        assertEquals("fizz", stemmer.stemWord("fizzed"));
        assertEquals("fail", stemmer.stemWord("failing"));
        assertEquals("file", stemmer.stemWord("filing"));
        assertEquals("happi", stemmer.stemWord("happy"));
        assertEquals("sky", stemmer.stemWord("sky"));
        assertEquals("relat", stemmer.stemWord("relational"));
        assertEquals("condit", stemmer.stemWord("conditional"));
        assertEquals("ration", stemmer.stemWord("rational"));
        assertEquals("valenc", stemmer.stemWord("valency"));
        assertEquals("digit", stemmer.stemWord("digitizer"));
        assertEquals("conform", stemmer.stemWord("conformably"));
        assertEquals("radic", stemmer.stemWord("radically"));
        assertEquals("differ", stemmer.stemWord("differently"));
        assertEquals("vile", stemmer.stemWord("vilely"));
        assertEquals("analog", stemmer.stemWord("analogously"));
        assertEquals("vietnam", stemmer.stemWord("vietnamization"));
        assertEquals("predic", stemmer.stemWord("predication"));
        assertEquals("oper", stemmer.stemWord("operator"));
        assertEquals("feudal", stemmer.stemWord("feudalism"));
        assertEquals("decis", stemmer.stemWord("decisiveness"));
        assertEquals("hope", stemmer.stemWord("hopefulness"));
        assertEquals("callous", stemmer.stemWord("callousness"));
        assertEquals("formal", stemmer.stemWord("formality"));
        assertEquals("triplic", stemmer.stemWord("triplicate"));
        assertEquals("form", stemmer.stemWord("formative"));
        assertEquals("formal", stemmer.stemWord("formalize"));
        assertEquals("hope", stemmer.stemWord("hopeful"));
        assertEquals("good", stemmer.stemWord("goodness"));
        assertEquals("reviv", stemmer.stemWord("revival"));
        assertEquals("allow", stemmer.stemWord("allowance"));
        assertEquals("infer", stemmer.stemWord("inference"));
        assertEquals("airlin", stemmer.stemWord("airliner"));
        assertEquals("gyroscop", stemmer.stemWord("gyroscopic"));
        assertEquals("adjust", stemmer.stemWord("adjustable"));
        assertEquals("defens", stemmer.stemWord("defensible"));
        assertEquals("irrit", stemmer.stemWord("irritant"));
        assertEquals("replac", stemmer.stemWord("replacement"));
        assertEquals("adjust", stemmer.stemWord("adjustment"));
        assertEquals("depend", stemmer.stemWord("dependent"));
        assertEquals("adopt", stemmer.stemWord("adoption"));
        assertEquals("homolog", stemmer.stemWord("homologous"));
        assertEquals("commun", stemmer.stemWord("communism"));
        assertEquals("activ", stemmer.stemWord("activate"));
        assertEquals("angular", stemmer.stemWord("angularity"));
        assertEquals("homolog", stemmer.stemWord("homologous"));
        assertEquals("effect", stemmer.stemWord("effective"));
        assertEquals("bowdler", stemmer.stemWord("bowdlerize"));
        assertEquals("probat", stemmer.stemWord("probate"));
        assertEquals("rate", stemmer.stemWord("rate"));
        assertEquals("ceas", stemmer.stemWord("cease"));
        assertEquals("control", stemmer.stemWord("controll"));
        assertEquals("roll", stemmer.stemWord("roll"));
    }
}
