package util;

import hash.IRHashMap;
import hash.IRHashSet;
import hash.StringHashCoder;
import index.Document;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;


/**
 * Utility file to read/write files.
 * @author andree.surya
 */
public class FileUtil {
    
    public static Set<String> readStopFile(String fileName) throws FileNotFoundException {
        final int SET_CAPACITY = 470; // Expected number of stop words.
        
        Set<String> stopWords = new IRHashSet<String>(SET_CAPACITY, new StringHashCoder());
        Scanner scanner = new Scanner(new FileInputStream(fileName));
        
        while (scanner.hasNextLine()) {
            String stopWord = scanner.nextLine().trim();
            stopWords.add(stopWord);
        }
        
        scanner.close();
        return stopWords;
    }
    
    /**
     * Write document map (docId-docNo) to a CSV file.
     * @throws FileNotFoundException 
     */
    public static void writeDocumentMap(String fileName, Map<Integer, Document> map) 
            throws FileNotFoundException {
        
        PrintWriter writer = new PrintWriter(fileName);
        for (Map.Entry<Integer, Document> entry : map.entrySet()) {
            Document doc = entry.getValue();
            writer.printf("%d,%s,%.3f,%d\n", 
                    doc.getId(), doc.getNumber(), 
                    doc.getWeight(), doc.getFilePointer());
        }
        
        writer.close();
    }
    
    /**
     * Read document map from the given CSV file.
     * @throws FileNotFoundException 
     */
    public static Map<Integer, Document> readDocumentMap(String fileName) throws FileNotFoundException {
        final int EXPECTED_SIZE = 10000; // Expected size of number of documents.
        
        Scanner scanner = new Scanner(new FileInputStream(fileName));
        Map<Integer, Document> documentMap = new IRHashMap<Integer, Document>(EXPECTED_SIZE);
        
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            StringTokenizer tokenizer = new StringTokenizer(line, ",");
            
            Integer key = Integer.parseInt(tokenizer.nextToken());
            Document doc = new Document();
            
            doc.setNumber(tokenizer.nextToken());
            doc.setWeight(Float.parseFloat(tokenizer.nextToken()));
            doc.setFilePointer(Long.parseLong(tokenizer.nextToken()));
            
            documentMap.put(key, doc);
        }
        
        scanner.close();
        return documentMap;
    }
    
    /**
     * Read lexicon from the given CSV file.
     * @throws FileNotFoundException 
     */
    public static Map<String, Long> readLexiconFile(String lexiconFile) throws IOException {
        final int EXPECTED_SIZE = 10000; // Expected size of lexicon
        
        FileInputStream in = null;
        BufferedReader reader = null;
        
        try {
            in = new FileInputStream(lexiconFile);
            reader = new BufferedReader(new InputStreamReader(in));
            
            Map<String, Long> lexicon = new IRHashMap<String, Long>(EXPECTED_SIZE);
            
            String line = reader.readLine();
            while (line != null) {
                StringTokenizer tokenizer = new StringTokenizer(line, ",");
                
                String term = tokenizer.nextToken();
                long filePointer = Long.parseLong(tokenizer.nextToken());
                
                lexicon.put(term, filePointer);
                line = reader.readLine();
            }
            
            return lexicon;
            
        } finally {
            if (reader != null) reader.close();
            if (in != null) in.close();
        }
    }
}
