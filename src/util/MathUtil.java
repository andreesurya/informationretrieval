package util;

/**
 * Utility class for some simple mathematical functions.
 * @author andree.surya
 */
public class MathUtil {

    public static long combination(int n, int k) {
        if (k * 2 > n) k = n - k;
        
        long result = 1;
        int nr = n - k;
        
        for (int i = n; i > k; i--) {
            result *= i;
            while (nr > 1 && result % nr == 0) result /= nr--;
        }
        
        while (nr > 1) result /= nr--;
        return result;
    }
}
