package util;

import index.Document;

import java.util.Collection;

/**
 * Set of functions related to Okapi BM25 document similarity calculations.
 * @author andree.surya
 */
public class OkapiUtil {
    // Some parameters for Okapi BM25 similarity functions.
    public static final float K1 = 1.2f;
    public static final float B = 0.75f;
    
    /**
     * Normalize the weights of the given documents.
     * Functions: K1 * ((1 - B) + B * Ld / AL)
     */
    public static void normalizeDocumentWeights(Collection<Document> docs) {
        
        // Calculate average document weight.
        float averageWeight = 0;
        for (Document doc : docs) 
            averageWeight += doc.getWeight();
        averageWeight /= docs.size();
        
        // Normalize document weight to the average.
        for (Document doc : docs)
            doc.setWeight(K1 * ((1 - B) + B * doc.getWeight() / averageWeight));
    }
    
    /**
     * Calculate partial similarity score between a document and one term.
     * @param n Number of documents in collection.
     * @param ft Number of documents containing this term.
     * @param fdt Number of occurences of this term in this document.
     * @param k Document weight pre-calculated during index generation.
     */
    public static float calculateSimilarity(int n, int ft, int fdt, float k) {
        return (float) Math.log((n - ft + 0.5) / (ft + 0.5)) * ((K1 + 1) * fdt / (k + fdt));
    }
}
