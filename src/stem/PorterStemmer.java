package stem;

/**
 * Word stemmer utility with Porter stemmer algorithm.
 * Reference: http://tartarus.org/martin/PorterStemmer/
 */
public class PorterStemmer {
    
    /**
     * Stem a word.
     */
    public String stemWord(String inputWord) {
        PorterString word = new PorterString(inputWord);
        
        stemStep1(word);
        stemStep2(word);
        stemStep3(word);
        stemStep4(word);
        stemStep5(word);
        
        return word.toString();
    }
    
    private void stemStep1(PorterString word) {
        
        int ln = word.length();
        boolean flag = false; // Flag for various use.
        
        // Step 1a.
        if (word.hasSuffix("sses")) { 
            word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ies")) { 
            word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ss")) {
            ; // Do nothing
        } else if (word.hasSuffix("s")) {
            if (ln > 2) word.deleteFrom(ln - 1);
        }
        
        // Step 1b.
        ln = word.length();
        flag = false;
        if (word.hasSuffix("eed")) {
            if (word.countM(ln - 3) > 0) word.deleteFrom(ln - 1);
        } else if (word.hasSuffix("ed")) {
            if (word.hasVowel(ln - 2)) {
                word.deleteFrom(ln - 2);
                flag = true;
            }
        } else if (word.hasSuffix("ing")) {
            if (word.hasVowel(ln - 3)) {
                word.deleteFrom(ln - 3);
                flag = true;
            }
        }
        
        if (flag) {
            ln = word.length();
            if (word.hasSuffix("at"))
                word.append('e');
            else if (word.hasSuffix("bl"))
                word.append('e');
            else if (word.hasSuffix("iz"))
                word.append('e');
            else if (word.hasDoubleConsonantEnd()) {
                char lastChar = word.charAt(ln - 1); 
                if (lastChar != 'l' && lastChar != 's' && lastChar != 'z')
                    word.deleteFrom(ln - 1);
            } else if (word.countM() == 1 && word.hasCvcEnd())
                word.append('e');
        }
        
        // Step 1c.
        ln = word.length();
        if (word.hasSuffix("y") && word.hasVowel(ln - 1))
            word.replaceFrom(ln - 1, "i");
    }

    private void stemStep2(PorterString word) {
        
        int ln = word.length();
        if (word.hasSuffix("ational")) {
            if (word.countM(ln - 7) > 0) word.replaceFrom(ln - 5, "e");
        } else if (word.hasSuffix("tional")) {
            if (word.countM(ln - 6) > 0) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("enci")) {
            if (word.countM(ln - 4) > 0) word.replaceFrom(ln - 1, "e");
        } else if (word.hasSuffix("anci")) {
            if (word.countM(ln - 4) > 0) word.replaceFrom(ln - 1, "e");
        } else if (word.hasSuffix("enci")) {
            if (word.countM(ln - 4) > 0) word.replaceFrom(ln - 1, "e");
        } else if (word.hasSuffix("izer")) {
            if (word.countM(ln - 4) > 0) word.deleteFrom(ln - 1);
        } else if (word.hasSuffix("bli")) {
            if (word.countM(ln - 3) > 0) word.replaceFrom(ln - 1, "e");
        } else if (word.hasSuffix("alli")) {
            if (word.countM(ln - 4) > 0) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("entli")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("eli")) {
            if (word.countM(ln - 3) > 0) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ousli")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ization")) {
            if (word.countM(ln - 7) > 0) word.replaceFrom(ln - 5, "e");
        } else if (word.hasSuffix("ation")) {
            if (word.countM(ln - 5) > 0) word.replaceFrom(ln - 3, "e");
        } else if (word.hasSuffix("ator")) {
            if (word.countM(ln - 4) > 0) word.replaceFrom(ln - 2, "e");
        } else if (word.hasSuffix("alism")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("iveness")) {
            if (word.countM(ln - 7) > 0) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("fulness")) {
            if (word.countM(ln - 7) > 0) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("ousness") ) {
            if (word.countM(ln - 7) > 0) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("aliti")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("iviti")) {
            if (word.countM(ln - 5) > 0) word.replaceFrom(ln - 3, "e");
        } else if (word.hasSuffix("biliti")) {
            if (word.countM(ln - 6) > 0) word.replaceFrom(ln - 5, "le");
        } else if (word.hasSuffix("logi")) {
            if (word.countM(ln - 4) > 0) word.deleteFrom(ln - 1);
        }
    }
    
    private void stemStep3(PorterString word) {
        
        int ln = word.length();
        if (word.hasSuffix("icate")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ative")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 5);
        } else if (word.hasSuffix("alize")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("iciti")) {
            if (word.countM(ln - 5) > 0) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ical")) {
            if (word.countM(ln - 4) > 0) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ful")) {
            if (word.countM(ln - 3) > 0) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ness")) {
            if (word.countM(ln - 4) > 0) word.deleteFrom(ln - 4);
        }
    }
    
    private void stemStep4(PorterString word) {
        
        int ln = word.length();
        if (word.hasSuffix("al")) {
            if (word.countM(ln - 2) > 1) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ance")) {
            if (word.countM(ln - 4) > 1) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("ence")) {
            if (word.countM(ln - 4) > 1) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("er")) {
            if (word.countM(ln - 2) > 1) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ic")) {
            if (word.countM(ln - 2) > 1) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("able")) {
            if (word.countM(ln - 4) > 1) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("ible")) {
            if (word.countM(ln - 4) > 1) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("ant")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ement")) {
            if (word.countM(ln - 5) > 1) word.deleteFrom(ln - 5);
        } else if (word.hasSuffix("ment")) {
            if (word.countM(ln - 4) > 1) word.deleteFrom(ln - 4);
        } else if (word.hasSuffix("ent")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ou")) {
            if (word.countM(ln - 2) > 1) word.deleteFrom(ln - 2);
        } else if (word.hasSuffix("ism")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ate")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("iti")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ous")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ive")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ize")) {
            if (word.countM(ln - 3) > 1) word.deleteFrom(ln - 3);
        } else if (word.hasSuffix("ion") && word.length() > 3) {
            char ch = word.charAt(ln - 4);
            if ((ch == 's' || ch == 't') && word.countM(ln - 3) > 1) 
                word.deleteFrom(ln - 3);
        }
    }
    
    private void stemStep5(PorterString word) {
        
        // Step 5a.
        int ln = word.length();
        if (word.hasSuffix("e")) {
           int m = word.countM(ln - 1);
           if (m > 1 || (m == 1 && ! word.hasCvcEnd(ln - 1)))
               word.deleteFrom(ln - 1);
        }
        
        // Step 5b.
        ln = word.length();
        if (word.hasSuffix("ll") && word.countM(ln - 1) > 1)
            word.deleteFrom(ln - 1);
    }
}
