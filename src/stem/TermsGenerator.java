package stem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Tool to generate terms from the given string.
 * Terms will be normalised, stemmed, and stop-words-filtered.
 */
public class TermsGenerator {
    private PorterStemmer stemmer = new PorterStemmer();
	private Set<String> stopWords = null;
	
	public void setStopWords(Set<String> stopWords) {
	    this.stopWords = stopWords;
	}
	
	/**
	 * Tokenize the given string (document/query) and generate list of terms.
	 */
	public List<String> createTerms(String string) {
		
		List<String> tokens = tokenizeString(string);
		List<String> terms = new LinkedList<String>();
		
		for (String token : tokens) {
			String term = createTerm(token);
			if (term != null) terms.add(term);
		}
		
		return terms;
	}
	
	/**
	 * Create a term from the given token.
	 * @return Term string or null if the token is considered as non-term.
	 */
	public String createTerm(String token) {
	    token = normaliseToken(token);
        
        // String length and stop-word check.
        if (token.isEmpty()) return null;
        if (stopWords != null && stopWords.contains(token)) return null;
        
        String term = stemmer.stemWord(token);
        return term.isEmpty() ? null : term;
	}
	
	/**
	 * Tokenize string on white-spaces.
	 * @return List of tokens.
	 */
	private List<String> tokenizeString(String string) {
		StringTokenizer tokenizer = new StringTokenizer(string);
		List<String> tokens = new ArrayList<String>(tokenizer.countTokens());
		
		while (tokenizer.hasMoreTokens())
			tokens.add(tokenizer.nextToken());
		
		return tokens;
	}
	
	/**
	 * Token normalisation (lower-casing, removal of non-letter words)
	 */
	private String normaliseToken(String token) {
	    StringBuilder builder = new StringBuilder(token);
	    
	    for (int i = 0; i < builder.length(); i++) {
            char ch = builder.charAt(i);
            
            if (Character.isUpperCase(ch))
                builder.setCharAt(i, Character.toLowerCase(ch));
            
            if (! Character.isLetter(ch)) {
                builder.deleteCharAt(i);
                i--;
            }
        }
	    
	    return builder.toString();
	}
}
