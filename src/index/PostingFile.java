package index;


import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * Represent an posting list file (a.k.a inverted list).
 * @author andree.surya
 */
public abstract class PostingFile {
    protected RandomAccessFile file;
    
    public PostingFile(String fileName) throws IOException {
        file = new RandomAccessFile(fileName, "rw");
    }
    
    /**
     * Write posting list to file in the specified position.
     * @return File pointer position at start of writing. 
     * @throws IOException 
     */
    public long writePostings(List<Posting> postings, long position) throws IOException {
        file.seek(position);
        return writePostings(postings);
    }
    
    /**
     * Read posting list from file in the given position.
     * @throws IOException 
     */
    public List<Posting> readPostings(long position) throws IOException {
        file.seek(position);
        return readPostings();
    }
    
    /**
     * Write posting list to file in current position.
     * @return File pointer position at start of writing. 
     * @throws IOException 
     */
    public abstract long writePostings(List<Posting> postings) throws IOException;
    
    /**
     * Read posting list from file in the current position.
     * @param count Number of postings to be read.
     * @throws IOException 
     */
    public abstract List<Posting> readPostings() throws IOException;
    
    public void close() throws IOException {
        file.close();
    }
}
