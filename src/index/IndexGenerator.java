package index;

import hash.IRHashMap;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Class to generate inverted index (lexicon + posting list).
 * @author andree.surya
 */
public class IndexGenerator {
    private static final int INITIAL_CAPACITY = 10000;

    private boolean isCompressed;
    private Map<String, List<Posting>> dictionary;
    
    public IndexGenerator() { this(true); }
    
    public IndexGenerator(boolean isCompressed) { 
        this.isCompressed = isCompressed; 
        dictionary = new IRHashMap<String, List<Posting>>(INITIAL_CAPACITY);
    }
    
    public void addDocument(int docId, List<String> terms) {
        
        for (String term : terms) {
            List<Posting> postingList = dictionary.get(term);
            
            if (postingList == null) { // Create posting list if not yet exists.
                postingList = new ArrayList<Posting>();
                dictionary.put(term, postingList);
            }
            
            // If last entry in the posting list correspond to docId, increment freq. 
            if (! postingList.isEmpty()) {
                Posting posting = postingList.get(postingList.size() - 1);
                
                if (posting.getDocID() == docId) {
                    posting.incrementFrequency();
                    continue; // Done, process next term.
                }
            }
            
            // If posting with docId doesn't currently exists in list, create one.
            postingList.add(new Posting(docId, 1));
        }
    }
    
    public void generateIndex(String lexiconFile, String invListFile) throws IOException {
        
        PrintWriter lexiconWriter = null;
        PostingFile postingFile = null;
        
        try {
            lexiconWriter = new PrintWriter(lexiconFile);
            postingFile = (isCompressed) ? 
                    new CompressedPostingFile(invListFile) : 
                    new PlainPostingFile(invListFile);
        
            for (Map.Entry<String, List<Posting>> entry : dictionary.entrySet()) {
                String term = entry.getKey();
                
                // Ensure postings in the list are ordered by ID.
                List<Posting> postingList = entry.getValue();
                Collections.sort(postingList, new PostingComparator());
                
                long pos = postingFile.writePostings(postingList);
                lexiconWriter.printf("%s,%d\n", term, pos);
            }
            
        } finally {
            if (lexiconWriter != null) lexiconWriter.close();
            if (postingFile != null) postingFile.close();
        }
    }
    
    /**
     * Class to compare between postings by ID.
     * @author andree.surya
     */
    private class PostingComparator implements Comparator<Posting> {

        @Override
        public int compare(Posting p1, Posting p2) {
            int i1 = p1.getDocID();
            int i2 = p2.getDocID();
            
            return (i1 > i2) ? 1 : (i1 < i2) ? -1 : 0;
        }
    }
}
