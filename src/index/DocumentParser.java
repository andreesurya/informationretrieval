package index;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathException;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Class to parse collection of documents in LATIMES file.
 */
public class DocumentParser {
    private static final String DOC_OPEN_TAG = "<DOC>";
    private static final String DOC_CLOSE_TAG = "</DOC>";
    
    private RandomAccessFile file;
    private DocumentBuilder documentBuilder;
    private XPathExpression docIdExpression;
    private XPathExpression docNoExpression;
    private XPathExpression textExpression;
    
    public DocumentParser(String fileName) throws FileNotFoundException, ParserConfigurationException, XPathException {
        file = new RandomAccessFile(fileName, "r");
        
        documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        XPath xPath =  XPathFactory.newInstance().newXPath();
        docIdExpression = xPath.compile("//DOCID/text()");
        docNoExpression = xPath.compile("//DOCNO/text()");
        textExpression = xPath.compile("//HEADLINE/P/text() | //TEXT/P/text()");
    }
    
    /**
     * @param pos Starting position from which to seek the next document.
     * @return The next document available in the collection file or null. 
     */
    public Document nextDocument(long pos) throws IOException, SAXException, XPathException {
        file.seek(pos);
        return nextDocument();
    }
    
    /**
     * @return The next document available in the collection file or null. 
     */
    public Document nextDocument() throws IOException, SAXException, XPathException {

        StringBuilder buffer = null; // Buffer to collect lines from <DOC> to </DOC>.
        long documentPointer = -1;
        long lastPointer = file.getFilePointer();
        
        String line = file.readLine();
        while (line != null) {            
            // Start buffering from <DOC> tag.
            if (buffer == null && DOC_OPEN_TAG.equals(line)) {
                buffer = new StringBuilder();
                documentPointer = lastPointer;
            }
            
            // Append to buffer while within <DOC></DOC> tags.
            if (buffer != null) buffer.append(line);
            
            // Stop buffering at </DOC> tag.
            if (DOC_CLOSE_TAG.equals(line)) break;
            
            lastPointer = file.getFilePointer();
            line = file.readLine();
        }
        
        // If we reach end of document or no buffered input, return nothing.
        if (line == null || buffer == null) return null;
        
        // Create document object from XML string.
        Document nextDocument = createDocument(buffer.toString());
        nextDocument.setFilePointer(documentPointer);
        
        return nextDocument;
    }
    
    /**
     * Convert the given xml string into a document.
     */
    private Document createDocument(String xmlString) throws IOException, SAXException, XPathException {
        Document document = new Document();
        
        // Parse XML documents and query the values we're interested in.
        InputSource xmlSource = new InputSource(new StringReader(xmlString));
        org.w3c.dom.Document xml = documentBuilder.parse(xmlSource);
        
        String docId = (String) docIdExpression.evaluate(xml, XPathConstants.STRING);
        String docNo = (String) docNoExpression.evaluate(xml, XPathConstants.STRING);
        
        document.setId(Integer.parseInt(docId.trim()));
        document.setNumber(docNo.trim());
        
        StringBuilder textBuilder = new StringBuilder("");
        NodeList textList = (NodeList) textExpression.evaluate(xml, XPathConstants.NODESET);
        
        for (int i = 0; i < textList.getLength(); i++)          
            textBuilder.append(textList.item(i).getNodeValue());
        
        document.setText(textBuilder.toString());
        document.setWeight(textBuilder.length());
        
        return document;
    }
    
    public void close() throws IOException {
        file.close();
    }
}
