package index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlainPostingFile extends PostingFile {

    public PlainPostingFile(String fileName) throws IOException {
        super(fileName);
    }

    public long writePostings(List<Posting> postings) throws IOException {
        long currentPosition = file.getFilePointer();
        
        file.writeInt(postings.size());
        for (Posting posting : postings) {
            int id = posting.getDocID();
            int freq = posting.getFrequency();
            
            file.writeInt(id);
            file.writeInt(freq);
        }
        
        return currentPosition;
    }

    @Override
    public List<Posting> readPostings() throws IOException {
        int count = file.readInt();
        List<Posting> postings = new ArrayList<Posting>(count);
        
        for (int i = 0; i < count; i++) {
            int id = file.readInt(); 
            int freq = file.readInt();
            
            postings.add(new Posting(id, freq));
        }
        
        return postings;
    }
}
