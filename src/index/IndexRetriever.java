package index;

import hash.IRHashMap;
import hash.IRHashSet;
import hash.StringHashCoder;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.xpath.XPathException;

import org.xml.sax.SAXException;

import data_structure.MinHeap;

import stem.TermsGenerator;
import util.MathUtil;
import util.OkapiUtil;

/**
 * Tool to read inverted index (lexicon + invlist)
 * @author andree.surya
 */
public class IndexRetriever implements Closeable {
    // Extra terms (result of expansion) are further weighted by this value.
    private static final float EXTRA_TERMS_WEIGHT = 0.2f;

    private DocumentParser documentParser;
    private PostingFile postingFile;
    private TermsGenerator termsGenerator;
    private Map<String, Long> lexicon;
    private Map<Integer, Document> documentMap;
    
    public void setDocumentParser(DocumentParser docParser) { this.documentParser = docParser; }
    public void setPostingFile(PostingFile postingFile) { this.postingFile = postingFile; }
    public void setTermsGenerator(TermsGenerator generator) { this.termsGenerator = generator; }
    public void setLexicon(Map<String, Long> lexicon) { this.lexicon = lexicon; }
    public void setDocumentMap(Map<Integer, Document> map) { this.documentMap = map; }
    
    /**
     * Expand the given query using the given parameters.
     * @param terms The terms to be expanded.
     * @param docCount Number of documents from which expanded terms will be selected.
     * @param termsCount Number of extra terms to be selected.
     * @return Expanded terms
     */
    public List<String> expandTerms(List<String> terms, int docCount, int termsCount) throws IOException, SAXException, XPathException {
        // Initial search for relevant documents.
        List<SearchResult> results = searchTerms(terms, null, docCount);
        
        Map<String, Integer> termCounters = 
                new IRHashMap<String, Integer>(docCount * 10, new StringHashCoder());
        
        for (SearchResult result : results) {
            long filePointer = result.getDocument().getFilePointer();
            Document doc = documentParser.nextDocument(filePointer);
            
            // Get unique terms from this document.
            List<String> docTerms = termsGenerator.createTerms(doc.getText());
            Set<String> uniqueTerms = new IRHashSet<String>(docTerms.size() / 5);
            uniqueTerms.addAll(docTerms);
            
            // Record occurences of these terms.
            for (String term : uniqueTerms) {
                // If this term is already in the original terms list, ignore.
                if (terms.contains(term)) continue;
                
                Integer counter = termCounters.get(term);
                if (counter == null) counter = 0;
                
                counter += 1;
                termCounters.put(term, counter);
            }
        }
        
        // Min-heap to collect terms with the lowest TSV. 
        MinHeap minHeap = new MinHeap(termsCount);
        
        // Calculate TSV for each terms.
        for (Map.Entry<String, Integer> entry : termCounters.entrySet()) {
            String term = entry.getKey();
            
            Long filePointer = lexicon.get(term);
            if (filePointer == null) continue;
            
            int n = documentMap.size();
            int ft = postingFile.readPostings(filePointer).size();
            int rt = Math.round(entry.getValue());
            int r = results.size();
            
            float value = (float) Math.pow(ft / (float) n, rt) * MathUtil.combination(r, rt);
            minHeap.insert(new TSV(term, value));
        }
        
        @SuppressWarnings("rawtypes")
        Comparable[] comparables = minHeap.get();
        
        List<String> extraTerms = new ArrayList<String>(termsCount);
        for (int i = 0; i < comparables.length; i++)
            extraTerms.add(((TSV) comparables[i]).term);
        
        return extraTerms;
    }
    
    /**
     * Search through the indexed documents for the given terms.
     * @param terms Terms based on which documents are searched.
     * @param extraTerms Terms from expansion result which will be assigned less weight.
     * @param count Number of search results to be returned.
     */
    public List<SearchResult> searchTerms(
            List<String> terms, List<String> extraTerms, int count) throws IOException {
        
        List<String> combinedTerms = new ArrayList<String>(terms);
        if (extraTerms != null) combinedTerms.addAll(extraTerms);
        
        // Accumulators to hold partial similarity scores of documents.
        Map<Integer, SearchResult> accumulators = 
                new IRHashMap<Integer, SearchResult>(documentMap.size() / 10);
        
        // Process each terms, filling in score accumulators.
        for (int i = 0; i < combinedTerms.size(); i++) {
            String term = combinedTerms.get(i);
            
            Long filePointer = lexicon.get(term);
            if (filePointer == null) continue;
            
            List<Posting> postings = postingFile.readPostings(filePointer);
            for (Posting posting : postings) {
                int docID = posting.getDocID();
                
                SearchResult result = accumulators.get(docID);
                if (result == null) {
                    Document document = documentMap.get(docID);
                    result = new SearchResult(document, 0f);
                    
                    accumulators.put(docID, result);
                }
                
                int n = documentMap.size();
                int ft = postings.size();
                int fdt = posting.getFrequency();
                float k = result.getDocument().getWeight();
                
                // Accumulate score from Okapi BM25 similarity calculation.
                float score = OkapiUtil.calculateSimilarity(n, ft, fdt, k);
                
                // If this term is an expansion result, further weight down score.
                if (i >= terms.size()) score *= EXTRA_TERMS_WEIGHT;
                
                result.addScore(score);
            }
        }
        
        MinHeap minHeap = new MinHeap(count);
        for (SearchResult searchResult : accumulators.values())
            minHeap.insert(searchResult);
        
        @SuppressWarnings("rawtypes")
        Comparable[] comparables = minHeap.get();
        
        List<SearchResult> topResults = new ArrayList<SearchResult>(comparables.length);
        for (int i = 0; i < comparables.length; i++)
            topResults.add((SearchResult) comparables[i]);
        
        Collections.sort(topResults, Collections.reverseOrder());
        return topResults;
    }
    
    /**
     * @see searchTerms
     */
    public List<SearchResult> searchTerms(List<String> terms, int count) throws IOException {
        return searchTerms(terms, null, count);
    }

    @Override
    public void close() throws IOException {
        postingFile.close();
    }
    
    private class TSV implements Comparable<TSV> {
        String term; float value; 
        
        public TSV(String term, float value) {
            this.term = term;
            this.value = value;
        }
        
        @Override
        public int compareTo(TSV other) {
            return (value < other.value) ? 1 : (value > other.value) ? -1 : 0;
        }
    }
}
