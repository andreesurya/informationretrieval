package index;

/**
 * One result from a search.
 * @author andree.surya
 */
public class SearchResult implements Comparable<SearchResult> {

    private Document document;
    private float score;
    
    public SearchResult(Document document, float score) {
        this.document = document;
        this.score = score;
    }
    
    public Document getDocument() { return document; }
    public float getScore() { return score; }
    
    public void setScore(float score) { this.score = score; }
    public void addScore(float score) { this.score += score; }
    
    @Override
    public int compareTo(SearchResult other) {
        return (score > other.score) ? 1 : (score < other.score) ? -1 : 0; 
    }
}
