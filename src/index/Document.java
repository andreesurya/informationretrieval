package index;

/**
 * Represents a document within LATIMES collection.
 */
public class Document {
    private int id = 0;
    private long filePointer = 0;
    private float weight = 0;
    private String number = null;
    private String text = null;
    
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    
    public long getFilePointer() { return filePointer; }
    public void setFilePointer(long filePointer) { this.filePointer = filePointer; }
    
    public float getWeight() { return weight; }
    public void setWeight(float weight) { this.weight = weight; }
    
    public String getNumber() { return number; }
    public void setNumber(String number) { this.number = number; }
    
    public String getText() { return text; }
    public void setText(String text) { this.text = text; }
}
