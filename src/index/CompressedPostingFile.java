package index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a posting list file, compressed using VB encoding scheme.
 * @author andree.surya
 */
public class CompressedPostingFile extends PostingFile {

    public CompressedPostingFile(String fileName) throws IOException {
        super(fileName);
    }

    @Override
    public long writePostings(List<Posting> postings) throws IOException {
        long currentPosition = file.getFilePointer();
        
        int lastId = 0;
        byte[] buffer = new byte[4];
        
        file.writeInt(postings.size());
        for (Posting posting : postings) {
            int id = posting.getDocID();
            int freq = posting.getFrequency();
            int idCode = id - lastId;
            
            int byteCount = encodeNumber(idCode, buffer);
            file.write(buffer, 0, byteCount);
            
            byteCount = encodeNumber(freq, buffer);
            file.write(buffer, 0, byteCount);
            
            lastId = id;
        }
        
        return currentPosition;
    }

    @Override
    public List<Posting> readPostings() throws IOException {
        int count = file.readInt();
        List<Posting> postings = new ArrayList<Posting>(count);
        
        int lastId = 0;
        int cNumber = 0;
        Posting cPosting = null;
        
        while (postings.size() < count) {
            byte cByte = file.readByte();
            
            if (cByte >= 0) { // Not finished reading a number, keep reading.
                cNumber = 128 * cNumber + cByte;
                continue;
            } else {
                cNumber = 128 * cNumber + (cByte & 127);
            }
            
            // Finished reading a number.
            if (cPosting == null) { // No current posting, create one. 
                lastId = lastId + cNumber;
                cPosting = new Posting(lastId, 0);
            } else {
                cPosting.setFrequency(cNumber);
                postings.add(cPosting);
                
                cPosting = null;
            }
            
            cNumber = 0;
        }
        
        return postings;
    }
    
    /**
     * Encode the given number using VB encoding and store in the given buffer.
     * @return The number of bytes required to represent the number with VB.
     */
    private int encodeNumber(int number, byte[] buffer) {
        int byteCount = (int) (Math.log(number) / Math.log(128) + 1);
        
        int i = byteCount - 1;
        while (true) {
            buffer[i] = (byte) (number % 128);
            if (number < 128) break;
            
            number = number / 128;
            i--;
        }
        
        buffer[byteCount - 1] |= 128; // Mark end of bytes with 1.        
        return byteCount;
    }
}
