package index;

/**
 * An object holding the document ID and within document frequency pairs.
 * @author andreas
 */
public class Posting {
	
	private int docId; // document ID
	private int frequency; // within document frequency
	
	/**
	 * @param docId Document ID
	 * @param frequency Within document frequency
	 */
	public Posting(int docId, int frequency) {
		this.docId = docId;
		this.frequency = frequency;
	}
	
	public Posting() { this(0, 0); }
	
	public int getDocID() { return docId; }
	public int getFrequency() { return frequency; }
	
	public void setDocId(int docId) { this.docId = docId; }
	public void setFrequency(int frequency) { this.frequency = frequency; }
	
	/**
     * Increasing within document frequency by 1
     */
    public void incrementFrequency () { frequency++; }
	
	@Override
	public String toString() {
		return "<" + docId + " , " + frequency + ">";
	}
}
