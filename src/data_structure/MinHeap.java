package data_structure;

public class MinHeap
{
	@SuppressWarnings("rawtypes")
	private Comparable[] container; // Array which hold the data
	private int size; // the number of the filled container
	
	public MinHeap (int capacity)
	{
		container = new Comparable[capacity + 1];
		size = 0;
	}
	
	/**
	 * Get an array of data
	 *  
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Comparable[] get()
	{
		Comparable[] returnContainer = new Comparable[size];
		for (int i = 0; i < size; i++)
		{
			returnContainer[i] = container[i+1];
		}
		return returnContainer;
	}
	
	/**
	 * Insert data to Heap
	 * 
	 * @param newData
	 */
	@SuppressWarnings("rawtypes")
	public void insert (Comparable newData)
	{
		if (size == (container.length-1))
			insertFull(newData);
		else
			insertNotFull(newData);
	}
	
	/**
	 * Insertion behavior when the container is not full yet.
	 * Any data are allowed to be inserted.
	 * 
	 * @param newData
	 */
	@SuppressWarnings("rawtypes")
	private void insertNotFull (Comparable newData)
	{
		int index = size + 1;
		container[index] = newData;
		size++;
		upHeapify(index);
	}
	
	/**
	 * Insertion behavior when we have full capacity on
	 * our MinHeap.
	 * 
	 * Our MinHeap will replace the root value with the newData
	 * only if it is the newData is greater.
	 * 
	 * @param newData
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void insertFull (Comparable newData)
	{
		if (newData.compareTo(container[1]) > 0)
		{
			// Only insert if the new data is greater than the smallest data
			container[1] = newData;
			downHeapify();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void downHeapify ()
	{
		int index = 1; // start from root
		int lastIndex = size / 2;
		Comparable tmp;
		
		while (index <= lastIndex)
		{
			int leftChild = index * 2;
			int rightChild = leftChild + 1;
			int swapChild;
			
			// Determine the smallest child
			if (rightChild > size)
			{
				swapChild = leftChild;
			}
			else
			{
				if (container[leftChild].compareTo(container[rightChild]) < 0)
					swapChild = leftChild;
				else
					swapChild = rightChild;
			}
			
			// Determine whether to continue heapify or not
			if (container[index].compareTo(container[swapChild]) > 0)
			{
				tmp = container[index];
				container[index] = container[swapChild];
				container[swapChild] = tmp;
				index = swapChild;
			}
			else
				break; // Heap Order Requirement already fulfilled.
		}
	}
	
	/**
	 * Used when we insert data when our container
	 * is not full yet.
	 * 
	 * @param index
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void upHeapify (int index)
	{
		int parent;
		Comparable tmp;
		
		while (index > 1)
		{
			parent = index / 2;
			if (container[parent].compareTo(container[index]) > 0)
			{
				// when parent > child, swap
				tmp = container[parent];
				container[parent] = container[index];
				container[index] = tmp;
				index = parent;
			}
			else
			{ // Heap Order Requirement already fulfilled.
				break;
			}
		}
	}
	
}
