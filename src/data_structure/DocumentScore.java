package data_structure;

public class DocumentScore implements Comparable<DocumentScore>
{
	private int docID;
	private double score;
	
	/**
	 * Data structure to be used in Heap
	 * 
	 * @param docID
	 * document identifier
	 * @param score
	 * document score from similarity function
	 */
	public DocumentScore (int docID, double score)
	{
		this.setDocID(docID);
		this.setScore(score);
	}
	
	@Override
	public int compareTo(DocumentScore o) 
	{
		// Compare based on the score value
		if (score > o.score)
			return 1;
		else if (score < o.score)
			return -1;
		else
			return 0;
	}

	public int getDocID() 
	{
		return docID;
	}

	public void setDocID(int docID) 
	{
		this.docID = docID;
	}

	public double getScore() 
	{
		return score;
	}

	public void setScore(double score) 
	{
		this.score = score;
	}

	@Override
	public String toString ()
	{
		return "<"+ docID + ":"+ score +">";
	}
}
