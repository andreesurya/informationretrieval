package hash;


import java.util.ArrayList;
import java.util.Collection;
import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;


/**
 * Hash Table with separate-chaining collision avoidance.
 * 
 * @author andreas
 * @param <K> Key type
 * @param <V> Value type
 */
public class IRHashMap<K, V> implements Map<K, V> {
    private static final double LOAD_FACTOR = 0.75; // 75% load factor
    private static final int DEFAULT_PRIME = 109345121;

    private Entry<K, V>[] buckets;
    private int size = 0; // Number of elements
    private HashCoder<K> hashCoder = null;

    // Numbers for hash value calculation.
    private int prime, scale, shift;

    public IRHashMap(int initialCapacity) {
        this(initialCapacity, null);
    }
    
    @SuppressWarnings("unchecked")
    public IRHashMap(int initialCapacity, HashCoder<K> hashCoder) {
        this.hashCoder = hashCoder;
        this.prime = DEFAULT_PRIME;

        buckets = (Entry<K, V>[]) new Entry[initialCapacity];

        // Setting up random number for variable important to hash coding
        Random rand = new Random(System.currentTimeMillis());
        scale = rand.nextInt(prime - 1) + 1;
        shift = rand.nextInt(prime);
    }

    /**
     * Set the object to be used to calculate hash. If not set, object's default
     * hashCode implementation will be used.
     */
    public void setHashCoder(HashCoder<K> hashCoder) {
        this.hashCoder = hashCoder;
    }

    @Override
    @SuppressWarnings("unchecked")
    public V get(Object key) {
        Entry<K, V> entry = findEntry((K) key);
        return (entry == null) ? null : entry.getValue();
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = new IRHashSet<K>(size);
        
        for (int i = 0; i < buckets.length; i++) {
            Entry<K, V> entry = buckets[i];
            while (entry != null) {
                keys.add(entry.getKey());
                entry = entry.next;
            }
        }
        
        return keys;
    }
    
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> entries = new IRHashSet<Map.Entry<K,V>>(size);
        
        for (int i = 0; i < buckets.length; i++) {
            Entry<K, V> entry = buckets[i];
            while (entry != null) {
                entries.add(entry);
                entry = entry.next;
            }
        }
        
        return entries;
    }
    
    @Override
    public Collection<V> values() {
        List<V> values = new ArrayList<V>(size);
        
        for (int i = 0; i < buckets.length; i++) {
            Entry<K, V> entry = buckets[i];
            while (entry != null) {
                values.add(entry.getValue());
                entry = entry.next;
            }
        }
        
        return values;
    }

    @Override
    public V put(K key, V value) {

        if (size >= buckets.length * LOAD_FACTOR)
            resizeBuckets(); // Rehash if number of items exceed threshold

        int bucketPos = calculateBucketPos(key);
        
        Entry<K, V> entry = buckets[bucketPos];
        Entry<K, V> previousEntry = null;
        
        // Keep iterating until end of chain / find matching key.
        while (entry != null) {
            previousEntry = entry;
            
            // Override value if matching key found.
            if (entry.getKey().equals(key)) {
                entry.setValue(value);
                return value;
            }
            
            entry = entry.next;   
        }
        
        Entry<K, V> newEntry = new Entry<K, V>(key, value);
        if (previousEntry == null) // Is this the first entry?
            buckets[bucketPos] = newEntry;
        else
            previousEntry.next = newEntry;
        
        size++;
        return value;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean containsKey(Object key) {
        Entry<K, V> entry = findEntry((K) key);
        return entry != null;
    }

    /**
     * Find entry with the corresponding key. Entry object will be returned or
     * null if not found.
     */
    private Entry<K, V> findEntry(K key) {

        int bucketPos = calculateBucketPos(key);
        Entry<K, V> entry = buckets[bucketPos];

        // Iterate simple linked list of entry.
        while (entry != null) {
            // Return immediately as soon as matching entry found.
            if (entry.getKey().equals(key))
                return entry;

            entry = entry.next;
        }

        return null;
    }
    
    /**
     * Calculate buckets position.
     */
    private int calculateBucketPos(K key) {
        int hashCode = (hashCoder == null) ? 
                key.hashCode() : hashCoder.hashCode(key);

        // MAD (Multiply-Add-Divide) formula.
        return (int) ((Math.abs(hashCode * scale + shift) % prime) % buckets.length);
    }

    @SuppressWarnings("unchecked")
    private void resizeBuckets() {
        size = 0;

        // Create new buckets with doubled capacity. 
        Entry<K, V>[] oldBuckets = buckets;
        
        int newCapacity = 2 * buckets.length;
        buckets = (Entry<K, V>[]) new Entry[newCapacity];

        // Setting up random number for variable important for hash coding.
        Random rand = new Random(System.currentTimeMillis());
        scale = rand.nextInt(prime - 1) + 1;
        shift = rand.nextInt(prime);

        // Replicate all entries from old buckets.
        for (int i = 0; i < oldBuckets.length; i++) {
            Entry<K, V> entry = oldBuckets[i];
            
            while (entry != null) {
                put(entry.getKey(), entry.getValue());
                entry = entry.next;
            }
        }
    }

    /**
     * Entry for IRHashMap, has the role to be a single linked list too, to
     * implement separate chaining collision avoidance.
     */
    public static class Entry<K, V> extends SimpleEntry<K, V> {
        private static final long serialVersionUID = -6705406403205971080L;

        // Pointer to next entry in the chain.
        private Entry<K, V> next;

        public Entry(K k, V v) {
            super(k, v);
            next = null;
        }
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public V remove(Object key) {
        throw new UnsupportedOperationException();
    }
}
