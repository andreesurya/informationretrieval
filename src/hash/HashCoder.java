package hash;

/**
 * Represents a tool object to calculate hash from data of the given type.
 * @author andree.surya
 * @param <T>
 */
public interface HashCoder<T> {
    public int hashCode(T data);
}
