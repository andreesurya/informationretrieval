package hash;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;


/**
 * Implementing a set using Hash with separate-chaining for collision avoidance. 
 * @author andreas
 */
public class IRHashSet<E> implements Set<E> {
    private static final double LOAD_FACTOR = 0.75; // 75% load factor
    private static final int DEFAULT_PRIME = 109345121;

    private Entry<E>[] buckets;
    private int size = 0; // Number of elements
    private HashCoder<E> hashCoder = null;

    // Numbers for hash value calculation.
    private int prime, scale, shift;
    
    public IRHashSet(int initialCapacity) {
        this(initialCapacity, null);
    }
    
    @SuppressWarnings("unchecked")
    public IRHashSet(int initialCapacity, HashCoder<E> hashCoder) {
        this.hashCoder = hashCoder;
        this.prime = DEFAULT_PRIME;

        buckets = (Entry<E>[]) new Entry[initialCapacity];

        // Setting up random number for variable important to hash coding
        Random rand = new Random(System.currentTimeMillis());
        scale = rand.nextInt(prime - 1) + 1;
        shift = rand.nextInt(prime);
    }
    
    /**
     * Set the object to be used to calculate hash. If not set, object's default
     * hashCode implementation will be used.
     */
    public void setHashCoder(HashCoder<E> hashCoder) {
        this.hashCoder = hashCoder;
    }
    
    @Override
    public boolean addAll(Collection<? extends E> elements) {
        boolean changed = false;
        
        for (E element : elements) {
            boolean added = add(element);
            if (added) changed = true;
        }
        
        return changed;
    }
    
    @Override
    public boolean add(E element) {
        
        if (size >= buckets.length * LOAD_FACTOR)
            resizeBuckets(); // Rehash if number of items exceed threshold
        
        int bucketPos = calculateBucketPos(element);
        
        Entry<E> entry = buckets[bucketPos];
        Entry<E> previousEntry = null;
        
        // Keep iterating until end of chain, check element existence in the set.
        while (entry != null) {
            previousEntry = entry;
            
            // Element already in the set, so do nothing.
            if (element.equals(entry.value)) return false;
            
            entry = entry.next;
        }
        
        Entry<E> newEntry = new Entry<E>(element);
        if (previousEntry == null) // Is this the first entry? 
            buckets[bucketPos] = newEntry;
        else 
            previousEntry.next = newEntry;
        
        size++;
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean contains(Object element) {
       int bucketPos = calculateBucketPos((E) element);
        
        Entry<E> entry = buckets[bucketPos];
        while (entry != null) {
            if (element.equals(entry.value)) return true;
            entry = entry.next;
        }
        
        return false;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
    
    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        List<E> list = new ArrayList<E>(size);
        
        for (int i = 0; i < buckets.length; i++) {
            Entry<E> entry = buckets[i];
            
            while (entry != null) { 
                list.add(entry.value);
                entry = entry.next;
            }
        }
        
        return list.iterator();
    }
    
    @SuppressWarnings("unchecked")
    private void resizeBuckets() {
        size = 0;

        // Create new buckets with doubled capacity. 
        Entry<E>[] oldBuckets = buckets;
        
        int newCapacity = 2 * buckets.length;
        buckets = (Entry<E>[]) new Entry[newCapacity];

        // Setting up random number for variable important for hash coding.
        Random rand = new Random(System.currentTimeMillis());
        scale = rand.nextInt(prime - 1) + 1;
        shift = rand.nextInt(prime);

        // Replicate all entries from old buckets.
        for (int i = 0; i < oldBuckets.length; i++) {
            Entry<E> entry = oldBuckets[i];
            
            while (entry != null) {
                add(entry.value);
                entry = entry.next;
            }
        }
    }
    
    /**
     * Calculate buckets position.
     */
    private int calculateBucketPos(E element) {
        int hashCode = (hashCoder == null) ? 
                element.hashCode() : hashCoder.hashCode(element);

        // MAD (Multiply-Add-Divide) formula.
        return (int) ((Math.abs(hashCode * scale + shift) % prime) % buckets.length);
    }
    
    /**
     * Represents an entry in IRHashSet + simple linked list to support
     * separate-chaining for collision avoidance.
     */
    public static class Entry<E> {
        private E value;
        private Entry<E> next;
        
        public Entry(E value) {
            this.value = value;
            this.next = null;
        }
    }

    @Override
    public boolean containsAll(Collection<?> arg0) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean remove(Object arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }
}
