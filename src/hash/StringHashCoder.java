package hash;

/**
 * Tool to calculate hash of the given string.
 * @author andree.surya
 */
public class StringHashCoder implements HashCoder<String> {

    @Override
    public int hashCode(String data) {
        int code = 0;
        for (int i = 0; i < data.length(); i++) {
            code = (code << 5) | (code >>> 27);
            code += (int) data.charAt(i);
        }
        
        return code;
    }
}
