package main;

import index.CompressedPostingFile;
import index.DocumentParser;
import index.IndexRetriever;
import index.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.xml.sax.SAXException;

import stem.TermsGenerator;
import util.FileUtil;

/**
 * Executable class to search for documents given query terms.
 */
public final class Search {
	public static void main(String[] args) throws IOException, ParseException, ParserConfigurationException, XPathException, SAXException {
		
	    // CLI arguments parsing.
	    ArgumentParser argParser = new ArgumentParser(args);
	    argParser.ensureValidArguments();
	    
	    String queryString = argParser.getQueryString();
	    String queryLabel = argParser.getQueryLabel();
	    int resultCount = argParser.getResultCount();
	    int expandCount = argParser.getExpandCount();
	    int termsCount = argParser.getTermsCount();
	    
	    TermsGenerator termsGenerator = new TermsGenerator();
	    IndexRetriever retriever = new IndexRetriever();
	    
	    // Inject all necessary helpers and data to retriever object.
	    retriever.setTermsGenerator(termsGenerator);
	    retriever.setPostingFile(new CompressedPostingFile(argParser.getInvListFile()));
	    retriever.setLexicon(FileUtil.readLexiconFile(argParser.getLexiconFile()));
	    retriever.setDocumentMap(FileUtil.readDocumentMap(argParser.getMapFile()));
        retriever.setDocumentParser(new DocumentParser(argParser.getCollectionFile()));
	    
	    // Use stop-words file if given.
	    if (argParser.isStopFileGiven())
            termsGenerator.setStopWords(FileUtil.readStopFile(argParser.getStopFile()));
	    
	    // Generate terms from query string.
	    List<String> terms = termsGenerator.createTerms(queryString);
	    if (argParser.isPrintEnabled()) printTerms("Original terms", terms); 
	    
	    List<String> extraTerms = null;
	    if (expandCount > 0) { // Do query expansion if needed.
	        extraTerms = retriever.expandTerms(terms, expandCount, termsCount);
	        if (argParser.isPrintEnabled()) printTerms("Extra terms", extraTerms);
	    }
	    
	    // Do searching with the given terms and expanded terms.
	    List<SearchResult> searchResults = retriever.searchTerms(terms, extraTerms, resultCount);
	    
	    // Print search results.
	    for (int i = 0; i < searchResults.size(); i++) {
	        SearchResult result = searchResults.get(i);
	        System.out.printf("%s %s %02d %f\n", queryLabel, 
	                result.getDocument().getNumber(), i + 1, result.getScore());
	    }
	    
	    retriever.close();
	}
	
	private static void printTerms(String label, List<String> terms) {
	    System.out.print(label + ": ");
	    
        for (String term : terms) 
            System.out.print(term + ' ');
        
        System.out.println("");
	}
	
	/**
     * Tool for argument parsing.
     * @author andree.surya
     */
    private static class ArgumentParser {
        private CommandLine cmd;
        private String[] args;
        
        public ArgumentParser(String[] args) throws ParseException {            
            Options options = new Options();
            
            options.addOption("s", true, "Specify the stop-words file to be used");
            options.addOption("q", true, "Label used to tag search result");
            options.addOption("n", true, "Number of search results to be returned");
            options.addOption("r", true, "Number of top documents considered for query expansion");
            options.addOption("e", true, "Number of expansion terms for query expansion");
            options.addOption("BM25", false, "Use Okapi BM25 similarity function");
            options.addOption("p", false, "Print debug messages");
            
            CommandLineParser parser = new PosixParser();
            this.cmd = parser.parse(options, args);
            this.args = cmd.getArgs();
        }
        
        public int getResultCount() {
            return cmd.hasOption('n') ? Integer.parseInt(cmd.getOptionValue('n')) : 20; 
        }
        
        public String getQueryLabel() { 
            return cmd.hasOption('q') ? cmd.getOptionValue('q') : "0";
        }
        
        public int getExpandCount() {
            return cmd.hasOption('r') ? Integer.parseInt(cmd.getOptionValue('r')) : 0;
        }
        
        public int getTermsCount() {
            return cmd.hasOption('e') ? Integer.parseInt(cmd.getOptionValue('e')) : 25;
        }
        
        public boolean isPrintEnabled() { return cmd.hasOption('p'); }
        public boolean isStopFileGiven() { return cmd.hasOption('s'); }
        public String getStopFile() { return cmd.getOptionValue('s'); }
        
        public String getLexiconFile() { return args[0]; }
        public String getInvListFile() { return args[1]; }
        public String getMapFile() { return args[2]; }
        public String getCollectionFile() { return args[3]; }
        
        public String getQueryString() {
            StringBuilder builder = new StringBuilder();
            for (int i = 4; i < args.length; i++) {
                builder.append(args[i]);
                builder.append(' ');
            }
            
            return builder.toString();
        }
        
        public void ensureValidArguments() {
            List<String> errors = new ArrayList<String>();
            
            if (args.length < 1) errors.add("Missing argument: <lexicon>");
            if (args.length < 2) errors.add("Missing argument: <invlist>");
            if (args.length < 3) errors.add("Missing argument: <map>");
            if (args.length < 4) errors.add("Missing argument: <collection>");
            if (args.length < 5) errors.add("Missing argument: <query_tokens>");
            
            for (String error : errors)
                System.err.println(error);
            
            if (! errors.isEmpty()) System.exit(1);
        }
    }
}
