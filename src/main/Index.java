package main;

import java.util.List;
import java.util.Map;

import hash.IRHashMap;
import index.Document;
import index.DocumentParser;
import index.IndexGenerator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import stem.TermsGenerator;
import util.FileUtil;
import util.OkapiUtil;

/**
 * Executable class to generate index files for IR.
 */
public final class Index {
	public static void main(String[] args) throws Exception {
	    
	    // CLI arguments parsing.
	    ArgumentParser argParser = new ArgumentParser(args);
	    argParser.ensureValidArguments();
	    
		DocumentParser docParser = new DocumentParser(argParser.getCollectionFile());
		TermsGenerator termsGenerator = new TermsGenerator();
		IndexGenerator indexGenerator = new IndexGenerator();
		Map<Integer, Document> documentMap = new IRHashMap<Integer, Document>(10000);
		
		// Use stop-words file if given.
		if (argParser.isStopFileGiven()) 
		    termsGenerator.setStopWords(FileUtil.readStopFile(argParser.getStopFile()));
		
		// Parse and process document by document.
		Document doc = docParser.nextDocument();
		while (doc != null) { 
		    // Generate list of terms from document's headline and text.
		    List<String> terms = termsGenerator.createTerms(doc.getText());
		    
		    // Index the terms within this document.
		    indexGenerator.addDocument(doc.getId(), terms);
		    documentMap.put(doc.getId(), doc);
		    
		    if (argParser.isPrintEnabled())
		        System.out.printf("Document indexed: %d\r", documentMap.size());
		    
		    doc = docParser.nextDocument();
		}
		
		// Normalize document weights for Okapi BM25 similarity functions.
		OkapiUtil.normalizeDocumentWeights(documentMap.values());
		
		// Output everything at once.
		indexGenerator.generateIndex("lexicon", "invlist");
		FileUtil.writeDocumentMap("map", documentMap);
		
		docParser.close();
	}
	
	/**
	 * Tool for argument parsing.
	 * @author andree.surya
	 */
	private static class ArgumentParser {
	    private CommandLine cmd;
	    private String[] args;
	    
	    public ArgumentParser(String[] args) throws ParseException {
	        Options options = new Options();
	        options.addOption("s", true, "Specify the stop-words file to be used");
	        options.addOption("p", false, "Print debug messages");
	        
	        CommandLineParser parser = new PosixParser();
	        this.cmd = parser.parse(options, args);
	        this.args = cmd.getArgs();
	    }
	    
	    public boolean isPrintEnabled() { return cmd.hasOption('p'); }
	    public boolean isStopFileGiven() { return cmd.hasOption('s'); }
	    public String getStopFile() { return cmd.getOptionValue('s'); }
	    public String getCollectionFile() { return args[0]; }
	    
	    public void ensureValidArguments() {
            if (args.length < 1) {
                System.err.println("Missing argument: <collection>");
                System.exit(1);
            }
	    }
	}
}
